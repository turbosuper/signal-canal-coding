Aufgabe 1

Starten  Sie  GNU  Octave  4.4  und  machen  Sie  sich  mit  der  graphischen  Oberfl ̈achevertraut. Suchen Sie sich im Internet ein Tutorial, um einen ersten Einblick zu be-kommen. Die offizielle Octave Online-Doku finden Sie unterhttps://www.gnu.org/software/octave/doc/interpreter. Nutzen Sie auch die interne Hilfe (z.B. Einga-behelp<befehl>im Command Window) von Octave, um Sprachelemente zu findenund zu verstehen.

Beim ̈Offnen  zeigt  der  File  Browser  auf  das  aktuelle  Arbeitsverzeichnis  bzw.  beimersten Start auf das Verzeichnis des Programmes selbst. ̈Andern Sie zu allererst dasArbeitsverzeichnis auf ihren pers ̈onlichen (gruppenspezifischen) Bereich auf dem Ser-ver.

GNU Octave verwendet die gleichen prim ̈aren GUI-Elemente (File Manager, Workspace,Command History, Command Window) wie Matlab. Im Command Window k ̈onnenBefehle  eingetragen  und  direkt  ausgef ̈uhrt  werden.  F ̈ur  die  Umsetzung  umfangrei-cherer  Algorithmen  empfiehlt  sich  das  Anlegen  von  Skripten  als  m-File  (entsprichtProgrammen bzw. Funktionen in anderen Programmiersprachen). m-Files bestehen1
im Wesentlichen aus einer Folge von Befehlen. Es ist hilfreich zu verstehen, dass Oc-tave grunds ̈atzlich alles als Matrizen behandelt, z.B. sind Vektoren Matrizen mit nureiner Zeile oder Spalte.

Erstellen Sie zur ̈Ubung mit dem integrierten Editor einige Skripte (Folge von Befeh-len) zur ̈Ubung und arbeiten Sie diese ab. Haben Sie keine Angst davor einfach einbisschen herumzuspielen. Machen Sie sich dann insbesondere damit vertraut wie man
1.  grundlegende Operationen anwendet, z.B. Berechnung einer Summe, eines Ska-larprodukts, Betrags, Potenzen, usw.
2.  Zufallszahlen generiert
3.  Schleifen und if-Abfragen umsetzt
4.  selbst Funktionen definiert und aufruft
5.  Funktionsgraphen plotten kann, z.B. eine Parabel, Sinusfunktion von 0...2π, ...
6.  Variablen oder Daten in Dateien speichert bzw. einliest und auf dem Bildschirmausgibt



Aufgabe 2

Nach Shannon ist die untere Grenze f ̈ur die mittlere Codewortl ̈ange (in Bits) f ̈ur dieCodierung  einer  Zufallsvariablen  durch  ihre  Entropie  gegeben.  Dies  soll  hier  n ̈aherer ̈ortert und am Beispiel untersucht werden. Zur Analyse wird die H ̈aufigkeitsvertei-lung grafisch mittels eines Histogramms dargestellt. Ein Histogramm ist ein S ̈aulen-diagramm, in dem gesammelte Daten zu Klassen zusammengefasst werden. Die Gr ̈oßeeiner S ̈aule entspricht dabei der Anzahl der Daten in einer Klasse
.1.  Erzeugen Sie einen Vektor gleichverteilter Zufallszahlen mit 5000 Samples. Stel-len Sie diese in einem Histogramm in 256 Klassen gleicher Breite dar und lassenSie sich die Anzahl der Elemente zur ̈uckgeben, die in jede Klasse f ̈allt. SchreibenSie dann eine einfache Funktion, um die Entropie der Verteilung∑μpμ·log2pμzu sch ̈atzen.μstellt die Zufallsvariable von Interesse dar. Welche Sch ̈atzewerteerhalten Sie?
2.  Wenn Sie die Anzahl der Klassen ̈andern, hat dieses auch einen Einfluss auf dieEntropiesch ̈atzung. Erl ̈autern Sie die Zusammenh ̈ange. Gehen Sie auch daraufein was passiert, wenn die Anzahl der Klassen beliebig groß gemacht werden.
3.  Erzeugen Sie einen neuen Vektor mit Zufallszahlen (gleiche Anzahl von Samples)und zeigen Sie wie sich die Entropie ̈andert. Erl ̈autern Sie die Hintergr ̈unde. Wiek ̈onnte  eine  allgemeing ̈ultige  untere  Grenze  f ̈ur  die  gesch ̈atzte  Codewortl ̈angeermittelt werden?
4.  In der ZIP-Datei zu diesem Labortermin sind zwei Bilddateien (im PPM-Format)enthalten. ̈Offnen Sie nacheinander beide Bilddateien in Octave und analysierenSie f ̈ur beide Bilddateien die Verteilung der Intensit ̈atswerte mittels Histogrammder Pixelwerte (wieder mit 256 Klassen). Berechnen Sie dann jeweils die Entro-pie des Bildes mit der von Ihnen bereits geschriebenen Funktion. Vergleichenund deuten Sie die Ergebnisse f ̈ur beide Bilder.
5.  Die Octave-Funktionecdfberechnet die empirische Verteilungsfunktion, im we-sentlichen entspricht diese dem Integral des Histogramms. Verwenden Sie dieseFunktion, um die kumulative Verteilung von beiden Bildern darzustellen. Dazum ̈ussen Sie die Bildvariable in einen Spaltenvektor umformen. Plotten Sie dieVerteilungsfunktion f ̈ur jedes Bild und vergleichen Sie das Ergebnis.
6.  Angenommen  Sie  m ̈ochten  die  Bilder  zum  Zweck  einer  effizienteren  Speiche-rung  oder ̈Ubertragung  komprimieren.  Beschreiben  Sie  ob  und  wie  mit  Hilfeder  empirischen  Verteilungsfunktion  eine  Kompression  der  Bilddaten  m ̈oglichw ̈are