% Funktion, der nimmt Elemente aus einer Vektor aus der cell array, 
%           und speichert die in einen Vektor ohne die Warhsceinlichkeit

% Funktion parameter:
%   carray   - cell_array mit Vectoren nach dieser Muster 
%              ['prTotal_1', 'Sym1_1', 'Sym1_2', 'Sym1_3'; 
%                 'prTotal_2', 'Sym2_1, 'Sym2_2', 'Sym2_3']
%   index     - welche Vektor aus der Cell array bearbeitet sien soll
% Ruckgabe
%   symbols_vec -  Vektor mit alle Symbole die zu dieser indexeriung 

function [symbols_vec] = create_sym_vec(index, carray);
    
    symbols_vec = [];
    count = size(carray)(2);
    for i = 2: count
     if (size(carray{index,i}))
      symbols_vec(i-1) = carray{index,i};
     endif
    endfor
  
endfunction