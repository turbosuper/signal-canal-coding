close all
home
clear

%read the file 
[sym, prob, codes] = textread("huff_codes.txt", "%d %f %s");

%calculate the length of each code
for i =1 :length(codes)
 exponents(i) = length(codes{i,1});
endfor

%prafix formula
sum = 0;
for j = 1 : length(exponents)
  sum = sum + 2^(-exponents(j));
endfor

