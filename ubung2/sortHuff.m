%Funktion der sortiert
%Parameter: 
%       prQ_S: zwei Vektroen, einer mit symbooeln, zweiter mit Wahrscheinlichkeit
%Ruckgabewert:       
%       sortedList - einer Vektor mit Symbolen die geordnet sind in fallende 
%                    Reihenfolge nach sein Wahrscheinlichketen
%       prVector - einer Vektor mit Warhsceinlichketen der Symboll geordnet

function [sortedList, prVector] = sortHuff (symbolVector, prQ)

  sortedList = []; % Initilisierung der Vektor
  prVector = [];  % Vektor fur Wahrschienlichketen
  
  %sortieren und in aray hinzufuegen
  for i = 1 : length(symbolVector)
    [maxPr, maxIndex] = max(prQ);
    if(maxPr == 0)
      break
    endif
    sortedList(i) = symbolVector(maxIndex);
    prVector(i) = maxPr;
    prQ(maxIndex) = -1.0;
  endfor;
   
endfunction
