%Funktion der addiert symbole zur Kodebuch. Funktion uberprueft ob die Symbol sind 
% shon da, und in dieser Fall addiert '1' oder '0' zu desen
% 
%Parameter: 
%       codwerod - derzeitige codebuch
%       symbols - Vektor mit die Symbole
%       bits 	- was zu dieser Symbole addiert sein soll. Muss '1' oder '0 sein
%Ruckgabewert:       
%       codeword - Kodebuch


function [codeword] = add_sym_to_codebook(codeword, symbols, bit)
  found_and_added = 0;
  bit_string = dec2bin(bit); %convert to string
  len_sym = length(symbols);
  code_vec = size(codeword)(1); %get the number of vectors in the codebook


  % check if symbols exist in codebook
  for i = 1 : len_sym %for every new symbol...
	  for k = 1 : code_vec %...go into each vector of coedebook..
  	  	if(symbols(i) == codeword{k,2}) %if element already exists
        old_code = codeword{k,(1)};
		    codeword{k,(1)} = strcat(bit_string, old_code);
		    found_and_added = 1;
		    endif
	 % endfor 
	  endfor
  endfor  
  if !(found_and_added) %if the symbol has not been found in the codebook
       codeword{end+1,(2)} = symbols(i);
 	     codeword{end,(1)} = bit_string;
  endif 
endfunction
