%Funktion der pack Symbolen mit deren Warcscheinlichketen
%Parameter: 
%      pr     - Vektor mit Wahrscheinlichkeiten
%      sym    - Vektor mit Symbolen 
%Ruckgabewert:       
%     sym_and_pr_carray - cell array mit Vektoren, deren letzter Wert ist der 
%                         Warschlinchkeit der Symbo


function  [sym_and_pr_carray] = prob_and_sym_to_carray(pr, sym)
  sym_and_pr_carray = {};
  for i = 1 : length(pr)
    sym_and_pr_carray(i,1) = sym(i);
    sym_and_pr_carray(i,2) = pr(i);
  endfor

endfunction