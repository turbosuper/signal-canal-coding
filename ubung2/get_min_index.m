%Funktion der findet minimale Warshceinlichkeit und deren index

% Argumente :
%   carray - cell array mit Warhsceinlichkeiten und symbole nach dieser Muster
%           { [ Pr1, S1], [Pr2, S2, S2_2, S2_3].....}
% Ruckgabewert:
%   minPr - minmale Warscheinlichket
%   Index der auftreten dieser Minamle Warhscleinlichte

function  [minPr, minIndex] = get_min_index(carray)
      %tc = { 3, "w", "r"; 2.5, "q", "ef"; 5, "p","l"; 0.5, "y", "q"};
      
      vector_size = size(carray)(1);
      prob_vec = [];
      %create vector form probabilities
      for i = 1 : vector_size-1
        prob_vec(i) = carray{(i),(1)};
      endfor
      
      %get the index
      [minPr, minIndex] = min(prob_vec);

endfunction