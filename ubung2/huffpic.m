% Funktion der spiecher Symbole mit deren Warhscheinlichkeiten und Kodierung in 
%         einer Datei
%
% Paramater:
%    codebook - Kodewort als Cell Array
%    prob - Wahrscheinlichkeiten als Vector
%    symb - Symbole als Vektoren, in die gleiche Reihenfolge als deren Warhscheinlichkeiten


function huffpic(codebook, prob, symb, filename)
  fid = fopen(filename, "w");
  fdisp(fid, "sym | prob | code");
  codebook_size = size(codebook)(1);
  for i = 1 : length(symb)
    current_sym = symb(i);
    current_prob = prob(i);
    for m = 1 : codebook_size
      if (codebook{(m),(2)} == current_sym)
        current_code = codebook{(m),(1)};
         fputs(fid, num2str(current_sym));
         fputs(fid, " ");
         fputs(fid, num2str(current_prob));
         fputs(fid, " ");
         fputs(fid, current_code);
         fputs(fid, "\n");
      endif
     endfor       
  endfor

  fclose(fid);
  

endfunction
