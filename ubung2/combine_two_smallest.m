%Funktion der kombiniert zwei kleinste Werte aus der Cell Array und machet eine codewort dazu
%Parameter: 
%       carray_sym_pr   - Cell Array mit Symbolen und deren Warhcsceinlichkeiten 
%Ruckgabewert:       
%       carray_sym_pr - bearbeitete cell array, wo der kleinste zwei zusmannegefasst
%                       sind, und Summe deren Warhscheinlichkeiten als letzter
%                       Werte in der enptscprechend Vektor geschrieben ist
%       codeword -  einer Cell Array mit die Kodwierte uals stringd und Symbole


function  [codeword, carray_sym_pr ] = combine_two_smallest(carray_sym_pr)
  codeword = {};

  
  %take the smallest pr value...
  [minPr, minIndex] = get_min_index(carray_sym_pr);
%  [minPr, minIndex] = min(cell2mat(carray_sym_pr(1:end)));
  smallest_sym = carray_sym_pr{minIndex,2};
  smallest_pr = carray_sym_pr{minIndex,1};
  temp_pr = smallest_pr; %temp value for the pair
  %...add the symbol to codeword 
  if (isempty(codeword)) %if the first one
    codeword{end+1,(2)} = smallest_sym;
    codeword{end,(1)} = "0";
  endif

  
  %... set the probabilty from the original vector to smth high
  carray_sym_pr{minIndex(1)}=[2];

  
  %... take the second smallest
  [minPr, minIndex] = get_min_index(carray_sym_pr);
%   [minPr, minIndex] = min(cell2mat(carray_sym_pr(1:end)));
  smallest_sym = carray_sym_pr{minIndex,2};
  smallest_pr = carray_sym_pr{minIndex,1};
  temp_pr = temp_pr+smallest_pr;
  % ... add the second smallest to codeword
  codeword{end+1,(2)} = smallest_sym;
  codeword{end,(1)} = "1";

endfunction