%Funktion der macht Huffman codierung

% Huffman
% Funktion der kodiert den Zeichenkette nach Huffman kodierung
%
% Parameter
% prQ       - Vektor mit Warhscheinlichkeiten
% SymVektS  - Vektor mit Symbole, deren Rehienfolge entspricht der Warshceinlichkeiten
%
% Ruckgabe
% kodierung - Cell Array mit Symbol und String, der Symbol kodiert in jede Cell


function [codeword] = huffman (prQ_S, SymVektS)
  
  codeword = {};
  
  %delete the values that have 0 probablity
  [sorted_list, sorted_prob] = sortHuff(SymVektS, prQ_S);
  
  %Probabilyt and Vecotrs in Cell Array
  [carray_sym_pr] = prob_and_sym_to_carray(sorted_list, sorted_prob);
  
  %create codebook
  [codeword, carray_sym_pr ] = create_codebook(carray_sym_pr);

  



endfunction