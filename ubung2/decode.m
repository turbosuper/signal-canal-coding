% Funktion der dekodiert das gespiecherte Bild mit einer ggb Kodebuch, und save
%         spiechert es in eine Datei
% 
% Parameter:
%        codebook - Kodebuch
%        file_in - Datei mit kodierter Bild
%        file_out - Datei mit dekodierter Bild, Farben als graustufenwerte


function  decode(codebook, file_in, file_out)
  fid = fopen(file_out, "w");

  %save file data as string
  coded_data = textread(file_in, "%s");
  coded_str = coded_data{(1)(1)};

  i = 1;
  window_end = 0;
  length(coded_str);
  
  while i <= length(coded_str) #iterate over coded string

    %set search window
   % if(i+window_end <= length(coded_str))
    string_window = coded_str(i:i+window_end);
   % endif
    %find a symbol for the code that is currently in search window
    for k = 1 : length(codebook) #search in the codebook
    codebook{(k),(1)};
    string_window;
       %if (codebook{(k),(1)} == string_window);
       if (strcmp(codebook{(k),(1)}, string_window)) %if symbol is found
        symbol = num2str(codebook{(k),(2)});
        fputs(fid, symbol);
        fputs(fid, " ");
        i = i+window_end+1;
        is_found = 1;
        is_found;
        symbol;
        window_end = 0;
        break;  
       else %if not found set a semaphore to widen the window
        is_found = 0;
       endif      
    endfor
    
    if !(is_found) #widen the window when symbol not found
        window_end++;
    else 
       window_end = 0;
    endif 


  endwhile
  
  
  fclose(fid);
  
endfunction
