%Funktion der addiert kombiniertes Vektor zu der ursprublicher cell Array
%Parameter: 
%       carray  - Cell Array mit Symbolen und deren Warhcsceinlichkeiten
%       new_vec - Vektor der addiert sein soll
%Ruckgabewert:       
%       carray_sym_pr - bearbeitete cell array, 

function [carray_sym_pr] = append_combined_sym(new_vec, carray)
 
  len = length(new_vec);
  carray{end+1,(1)} = new_vec(1);
  for i = 2 : len
      carray{end,(i)} = new_vec(i);
  endfor
  carray_sym_pr = carray;
  
endfunction