% Funktion, der kodiert Bild mit Huffman kodierung, und speichert es in eine Datei
%
% Paramater:
%          picture - Bild in ppm format
%          filename - Name der Datei wo kodiert Bild gespiechert sein soll
% Ruckgabewert:
%          codebook - Kodebuch als Cell Array

function [codebook] = encode(picture, filename);
pic = imread(picture);
pixel_count = size(pic)(1)*size(pic)(2);
fid = fopen(filename,"w"); %file to save

% convert to grayscale
pic = ( pic(:,:,1) .* 0.2126 ) + ( pic(:,:,2) .*  0.7152 ) + ( pic(:,:,3) .*  0.0722 );

%flip the vektor
pic_vec = pic(:)';

%for TEST only - create a file to easily compare the resultss
fid2 = fopen("not_encoded.txt","w");
for v = 1 : length(pic_vec)
  str = num2str(pic_vec(v));
  fputs(fid2, str); 
  fputs(fid2, " ");
endfor 
fclose(fid2);

%nn - numbers of occurances; xx - vector with symbols
[nn, xx] = hist(pic_vec,256); %da es gibt nur 254 Werte der Intesitat, man kann es nicht
                               %auf 254 verteilen

%calculate the probabilyt
sym_pr = nn ./ pixel_count; 

symb = xx;

[codebook] = huffman(sym_pr, symb);

%generate the file
for i = 1 : size(pic_vec)(1) #iterate over each column...
  for j = 1 : size(pic_vec)(2) #iterate over each row...
   current_symbol = pic_vec(i,j);
   for k = 1 : length(codebook)
       if (codebook{(k),(2)} == current_symbol);
        current_code = codebook{(k),(1)};
        %save to file
        fputs(fid, current_code);
        break;
      endif
     endfor   
  endfor
endfor  


fclose(fid);


endfunction