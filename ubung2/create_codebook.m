%Funktion der macht einer Kodebuch
%Parameter: 
%       carray_sym_pr   - Cell Array mit Symbolen und deren Warhcsceinlichkeiten 
%Ruckgabewert:       
%       carray_sym_pr - bearbeitete cell array, wo der kleinste zwei zusmannegefasst
%                       sind, und Summe deren Warhscheinlichkeiten als letzter
%                       Werte in der enptscprechend Vektor geschrieben ist
%       codeword -  einer Cell Array mit die Kodwierte uals stringd und Symbole


function  [codeword, carray_sym_pr ] = create_codebook(carray_sym_pr)
  codeword = {};
  carray_sym_pr;
  
  %initialise the codeword
  
  %take the smallest pr value...

  [minPr, minIndex] = get_min_index(carray_sym_pr);
  first_sym = carray_sym_pr{minIndex,2};
  first_pr = carray_sym_pr{minIndex,1};
  temp_pr = first_pr; %temp value for the pair
  %...add the symbol to codeword 
  codeword{end+1,(2)} = first_sym;
  codeword{end,(1)} = "0";
  codeword;
  
  %... set the probabilty from the original vector to smth high
  carray_sym_pr{minIndex(1)}=[2];
  
  %... take the second smallest

  [minPr, minIndex] = get_min_index(carray_sym_pr);
  second_sym = carray_sym_pr{minIndex,2};
  second_pr = carray_sym_pr{minIndex,1};
  temp_pr = temp_pr+second_pr;
  carray_sym_pr{minIndex(1)}=[2];
  % ... add the second smallest to codeword
  codeword{end+1,(2)} = second_sym;
  codeword{end,(1)} = "1";
  codeword;
  
  new_vec= [temp_pr, first_sym, second_sym];
  
  %add vector to the cell array
  [carray_sym_pr] = append_combined_sym(new_vec, carray_sym_pr);
  carray_sym_pr;
  %always take the two smallest
  while (new_vec(1) < 1)
    [minPr, minIndex] = get_min_index(carray_sym_pr);
    %get all the symbols taht have the lowest probablity
    [first_sym] = create_sym_vec(minIndex,carray_sym_pr);
    first_pr = carray_sym_pr{minIndex,1};
    temp_pr = first_pr;
    %...add the symbol to codeword 
    [codeword] = add_sym_to_codebook(codeword, first_sym, 1);
    carray_sym_pr{minIndex(1)}=[2];
  
    [minPr, minIndex] = get_min_index(carray_sym_pr);
    %take all the symbols taht have the lowest probablity
    second_sym = create_sym_vec(minIndex,carray_sym_pr);
    second_pr = carray_sym_pr{minIndex,1};
    temp_pr = temp_pr + second_pr;
    %...add the symbol to codeword 
    [codeword] = add_sym_to_codebook(codeword, second_sym, 0);
    carray_sym_pr{minIndex(1)}=[2]; %set it high = never to index again
    new_vec = [temp_pr, first_sym, second_sym];
    [carray_sym_pr] = append_combined_sym(new_vec, carray_sym_pr);
   carray_sym_pr;
  endwhile
  
endfunction