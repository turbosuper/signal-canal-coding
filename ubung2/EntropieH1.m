%symEntropie = EntropieH1(PrQ)
%Funktion der Erstellt Entropie nach der Verfahren H = prQ * log2(1/prQ) 
%Argumente: prQVektor - Wahsrscheinlicket der jeden Symbol in einer Vektor

function h1 = EntropieH1 (prQVektor)   
  h1 = 0;
  for i = 1 : length(prQVektor)
    if prQVektor(i) ~= 0
      a = log2(1/prQVektor(i));
      b = prQVektor(i)*a;
      h1 += b;
    endif
  endfor;
endfunction