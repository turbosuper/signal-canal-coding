clear
home
close all
disp("Starting Program....")

%I = imread ("samplepic0353.ppm");
%J = imread ("samplepic0498.ppm");
J = imread ("testbild.ppm");

%J = imread ("icons8.ppm");
I = J;
%imshow(I

%Octave lifert die Bilder als 3 Dimensionale Matrizzen
%Erste Dimension der Matrize ist der X Wert des Pixels
%Zweiter Dimenstion der Matrize ist der Y Wer des Pixels
%Dritter Wert des Matrizze ist der Intensitaet verteilet in drei Farben - RGB,
%also die Matrizze hat drei Ebene in der z Axis

%ZB darstellen nur  Werte der 'R' -erste ebene des Z-s Achse, fuer alle Pixelen
%imshow(I(:,:,1)) %beduete: alle x, alle y, aber nur erste aus der Z
%AUFPASSEN - Die Werte fuer das Intensiviteat sind umgekhret '0' steht fuer Maximimum!

%man berechnet der Intensitaet aus der Fformel (0.2126*R + 0.7152*G + 0.0722*B)

Iint = ( I(:,:,1) .* 0.2126 ) + ( I(:,:,2) .*  0.7152 ) + ( I(:,:,3) .*  0.0722 );
Jint = ( J(:,:,1) .* 0.2126 ) + ( J(:,:,2) .*  0.7152 ) + ( J(:,:,3) .*  0.0722 );
%das konvertiert das Bild in Greyscale. JEtzt jede Pixel hat eine Intensitaet
JintVekt = Jint(:)';
[nnJ, xxJ] = hist(JintVekt,254); 
prQJ = nnJ ./ 9216;          
H_J = EntropieH1(prQJ);
%um das auf hist darzustellen, muss man die Werte in einer Vektor speichern
IintVekt = Iint(:)';
%figure(1)
%hist(IintVekt,254);  
[nnI, xxI] = hist(IintVekt,255); %da es gibt nur 254 Werte der Intesitat, man kann es nicht
                               %auf 254 verteilen
prQI = nnI ./ 9216; 
H_I = EntropieH1(prQI);

SymbVektI = xxI;
SymbVektJ = xxJ;

%[kodierung_I] = huffman(prQI, SymbVektI);
%[noZeroS_I, noZeroW_I] = huffman(prQI, SymbVektI);

%huffpic(kodierung_I, prQI, SymbVektI, "huff_codes.txt");

[codebook] = encode("testbild.ppm", "coded_pic.txt");
codebook

decode(codebook, "coded_pic.txt", "decoded_pic.txt")


