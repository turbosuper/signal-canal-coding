%TODO
% - ab Aufgabe 1.3 weiter machen
%
clear
home
close all

%DCT generieren
base_size =4; %4, 8 oder 16 DCT pro Base
block_size = 8;

base = dct_base(base_size, block_size); 
base_ppm = base_2_ppm(base, "dct_blocks.ppm");
%test_dct = imread("dct_blocks.ppm");
%imshow(test_dct)

pic = imread("icons8.ppm");
%konvertiereen in Gfraustufen
%pic_int = ( J(:,:,1) .* 0.2126 ) + ( J(:,:,2) .*  0.7152 ) + ( J(:,:,3) .*  0.0722 );
pic_blocks = create_8x8_blocks(pic); 
coeff = dct_code(pic_blocks, 8);