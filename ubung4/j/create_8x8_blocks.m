%Funktion der konvertiert das Bild in 8x8 pixel gross bloecke
%
%Paramter
%  pic - eines Bild gespeichert als ppm Datei mit Farben kodiert in Grauenstufen
%        PIC MUSS TEILBAR DURCH 8 WERDEN - SPALTENWEISE UND ZEILENWEISE!!!!
%Ruckgabewert
%  pic_blocks - einen Cell Array mit Matrizen die 8x8 Werte haben

function [pic_blocks] = create_8x8_blocks(pic)
 %{
 Idee:
 Mit der X, Y Koordinaten, kann man die richtege Pixels auswahelne, nach dieser
 Muster: pic(erste_x:letzte_x, erste_y:letzte_y)
 Beispiel fuer der zweiter Block: pic(9:16,1:8)
 %}
 
 i =1;  %der X Koordinate des Pixels
 k =1;  %der Y Koordinate des pixels
 a = 1; %cell array index
 b = 1;
 pic_blocks = {};
 
 while i < size(pic)(1)%-8
  while k < size(pic)(2)%-8
 
  pic_blocks{b,a} = pic(i:i+7,k:k+7); %den richtigen Viereckauschneiden
  k = k+8; %Anfang der naschter Block mit 8 Pixel nach rechts schieben
  a++;
  endwhile
  k=1; % von linke Seite anfangen
  a=1;
  i = i+8; 
  b++;
 endwhile

endfunction