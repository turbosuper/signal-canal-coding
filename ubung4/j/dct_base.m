%Funktion der generiert DCT Werte fuer den gegebene groesse
%
%Paramater:
%     base_size - Anzahl der DCT Basisfunktionen
%     block_size - Anzahl der Pixel in jede block
%Ruckgabewerte
%     base - Cell Array mit Grauenstufen Werte fuer die generierter Bild

function [base] = dct_base(base_size,block_size) 

  %uber die Bloecke iterrieren
  for k1 =0 : base_size-1
  for k2 =0 : base_size-1
  
  %fuer jeder Block die DCT Muster erzeugen
  for i1 = 0: block_size-1
    for i2 = 0: block_size-1
    %DCT Werte generieren fuer dieser Block
    dct_matrix(i1+1,i2+1) = cos((2*i1+1)*k1*pi / (2*block_size)) * cos((2*i2+1)*k2*pi / (2*block_size) );
    endfor
  endfor
  
  %DCT Muster speicher an der richtigen Stelle
  base{k1+1,k2+1} = dct_matrix;
  endfor
  endfor
  
  
 base;

endfunction