%base_2_ppm

% Funktion, der konvertiert Cella Array mit DCT Basisfunktionen zur ppm

% Funktion parameter:
%   base   - Cell array mit Werte die konvertiert werden muss
%   file_name - name der Datei wo die einzelne Blocke gespeichert sein sollen
% Ruckgabe
%   base_ppm - Matrizze mit Werte in Grauenstufen Fromat

function [base_ppm] = base_2_ppm(base, file_name)
  fid = fopen(file_name, "w");
  gray_val = 255;
  
  %ins jede Block rein
  for k1 =1: length(base)
  for k2 =1: length(base)
  current_matrix = base{k1,k2};
  
  %jede Matrizze einzeln speichern
    for k =  1 : length(current_matrix)
      for l = 1 : length(current_matrix)
        %convert to grayscale
        current_val = current_matrix(l,k);
        current_val = current_val+1; %schieben der Amplitude
        current_val = (gray_val/2)*current_val; %da Amplitude schon 0-2, man
                                              % braucht nur halbe der Maximale
        current_val = uint8(current_val);
        base_ppm_matrix(l,k) = current_val;

      endfor
    endfor
    base_ppm{k1,k2} = base_ppm_matrix;
    %figure
    %imshow(base_ppm_matrix)
    fname = strcat ( "pic", num2str(k1), "x",  num2str(k2), ".png" );
    imwrite(base_ppm_matrix, fname);

    endfor
    endfor
    
    fclose(fid);
endfunction