%{
Funktion der kodiert Werte mit Discrete Cosinus Transform

Parameter:
  base_size - Anzahl der basis DCT Funktione
  blocks_carray - Bild in 8x8 Bloecke unterteilt, als Cell Array gespiechrt

Ruckgabewert:
 coffeicient_array - cella array mit Koeffizienten der DCT

 Voss Folien Seite 11, 6ter Folien
https://www.geeksforgeeks.org/discrete-cosine-transform-algorithm-program/
 %}

function [coefficient_array] = dct_code(blocks_carray, base_size)

for block_x = 1 : size(blocks_carray)(1) %uber die 8x8 Blocke der C array zeilenweisen iterieren
for block_y = 1 : size(blocks_carray)(2) %spaltennweisen iterieren
current_matrix = blocks_carray{block_x,block_y};
block_size=8;

%DCT Anwenden
  for k1 =0 : base_size-1
  for k2 =0 : base_size-1  
  
  for i1 = 0: block_size-1
    for i2 = 0: block_size-1
    coeff_matrix(k1+1,k2+1) = current_matrix(i1+1,i2+1)*(cos((2*i1+1)*k1*pi / (2*block_size)) * cos((2*i2+1)*k2*pi / (2*block_size) ));
    endfor
  endfor
   
  endfor
  endfor
  
coefficient_array{block_y,block_x} = coeff_matrix; %in die Ausgang speichern
   
endfor
endfor


%{
%uber die Bloecke iterrieren
  for k1 =0 : base_size-1
  for k2 =0 : base_size-1

  
  
  %fuer jeder Block die DCT Muster erzeugen
  for i1 = 0: block_size-1
    for i2 = 0: block_size-1
    %DCT Werte generieren fuer dieser Block
    dct_matrix(i1+1,i2+1) = cos((2*i1+1)*k1*pi / (2*block_size)) * cos((2*i2+1)*k2*pi / (2*block_size) );
    endfor
  endfor
  
  %DCT Muster speicher an der richtigen Stelle
  base{k1+1,k2+1} = dct_matrix;
  endfor
  endfor
      for (k = 0; k < m; k++) { 
                for (l = 0; l < n; l++) { 
                    dct1 = matrix[k][l] *  
                           cos((2 * k + 1) * i * pi / (2 * m)) *  
                           cos((2 * l + 1) * j * pi / (2 * n)); 
                    sum = sum + dct1; 
                } 
%}
endfunction 