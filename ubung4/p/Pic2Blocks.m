function pic_blocks = Pic2Blocks(pic, block_size)
  i =1;  %der X Koordinate des Pixels
 k =1;  %der Y Koordinate des pixels
 a = 1; %cell array index
 b = 1;
 max = block_size-1;
 dim=size(pic);
 pic_blocks = {};
 
 while i < dim(1)%-8
  while k < dim(2)%-8
 
  pic_blocks{b,a} = pic(i:i+max,k:k+max); %den richtigen Viereckauschneiden
  k = k+block_size; %Anfang der naschter Block mit 8 Pixel nach rechts schieben
  a++;
  endwhile
  k=1; % von linke Seite anfangen
  a=1;
  i = i+block_size; 
  b++;
 endwhile
endfunction
