%Aufagbe2 ist abhängig von Aufgabe1, welche die wichtigen Befehle bereits 
%ausführt und die Bildblöcke einfach zurück gibt.
[pic_blocks, dct_funcs] = Aufgabe1();
% Quantisieren
load quantmat.txt
dim_blocks=size(pic_blocks);
quant_blocks = pic_blocks;
for xdim = 1:dim_blocks(1)
  for ydim = 1:dim_blocks(2)
    quant_blocks{xdim, ydim} = DCT_quantise(quant_blocks{xdim, ydim}, quantmat, 0); % best value: 45.3216
  endfor
endfor

# reconstruct:
reco_blocks = quant_blocks;
for xdim = 1:dim_blocks(1)
  for ydim = 1:dim_blocks(2)
    inverseBlock = iDCT2d(reco_blocks{xdim, ydim}, dct_funcs);
    inverseBlock = floor(abs(inverseBlock));
    reco_blocks{xdim, ydim} = iDCT2d_normalizePic(inverseBlock);
  endfor
endfor

# rebuild Matrix of image
block_size = size(reco_blocks{1,1});
picmat=zeros(block_size(1)*dim_blocks(1), block_size(2)*dim_blocks(2));
block_size -= 1;
i=1;
k=1;
a=1;
b=1;
picdim = size(picmat);
while i < picdim(1)
  while k < picdim(2)
    picmat(i:i+block_size(1), k:k+block_size(2)) = reco_blocks{a, b};
    k += block_size(2)+1;
    b += 1;
  endwhile
  k = 1;
  b = 1;
  i += block_size(1)+1;
  a += 1;
endwhile
%picmat += 127.5; % to_unsigned_integer()
# display resulting image
imshow(uint8(picmat));