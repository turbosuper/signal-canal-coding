function outblock = iDCT(inblock, dcts)
  dim=size(inblock);
  outblock=zeros(dim(1));
  x0 = 0.5*inblock(1,1);
  outblock(1,1) = x0+inblock(1,1);
  k = 0 : dim(1)-1;
  for i = 1 : dim(1)
    for j = 1 : dim(2)
      if(i == 1) % don't use DC
        continue;
      endif
      %outblock(i,j) = (inblock(i,j) * dcts{i,j}(i,j) ) + x0;
      outblock(i,j) = inblock(i,j) / dcts{i,j}(i,j);
    endfor
  endfor
endfunction
