function outblock = iDCT2d(inblock, dcts)
  dim=size(inblock);
  %outblock=zeros(dim(1));
  x0 = 0.5*inblock(1,1)*dcts{1,1};%*sqrt(2/dim(1)); % add DC block
  outblock = x0+1;
  
  for i = 1 : dim(1)
    for j = 1 : dim(2)
      if((i == 1) && (j == 1)) % don't use DC
        %continue;
      else
        outblock += (0.5*inblock(i,j) * (dcts{i,j})*sqrt(2/dim(1)) )+0.5;  
      endif
      
      %outblock(i,j) = inblock(i,j) / dcts{i,j}(i,j);
    endfor
  endfor
endfunction
