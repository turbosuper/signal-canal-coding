function sum = DCT_Sum(block, bases)
  dim_bases=size(bases);
  sum=zeros(dim_bases(1));
  for xdim=1:dim_bases(1)
    for ydim=1:dim_bases(2)
      current_base = bases{xdim, ydim};
      %sum(1,1) = sum(1,1)*bases{1,1}(xdim,ydim); % mult DC - bases{1,1} goto dc matrix; followd by (xdim,ydim) goto pix value in dc matrix
      sum(xdim,ydim) = current_base(xdim,ydim)*block(xdim,ydim);
    endfor
  endfor
endfunction
