function DCT2d_pict(base)
  dim=size(base);
  %ins jede Block rein
  for k1 =1: dim(1)
    for k2 =1: dim(2)
      current_matrix = base{k1,k2};
      fname = strcat ( "pic", num2str(k1), "x",  num2str(k2), ".png" );
      imwrite(uint8((current_matrix+1)*127.5), fname); % alt.: abs(current_matrix+255)
    endfor
  endfor
endfunction