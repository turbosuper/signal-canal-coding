function outblock = iDCT2d_normalizePic(inblock)
  dim=size(inblock);
  outblock=inblock;
  for xdim = 1 : dim(1)
    for ydim=1:dim(2)
      if(inblock(xdim,ydim) > 255)
        outblock(xdim,ydim) = floor(inblock(xdim, ydim)/2);
      endif
    endfor
  endfor
endfunction
