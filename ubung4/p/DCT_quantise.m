function quantblock = DCT_quantise(block, f, n)
  divblock=block ./ f;
  block_rne = floor(divblock);
  quantblock = block_rne .* f;
  if(n > 0) % if set > 0, filter n max values
    dim_block=size(block);
    temp=zeros(dim_block(1));
    absblock=abs(quantblock);
    for i = 1 : n
      [maxV, someIndex] = max(max(absblock));
      [rows,cols] = find(absblock == maxV);
      for ptr = 1 : length(rows)
        row=rows(ptr);
        col=cols(ptr);
        temp(row, col) = quantblock(row, col);
        absblock(row, col) = -1000; % Set it to value that can never be a value - < -127 !
      endfor
    endfor
    quantblock=temp;
  endif
endfunction
