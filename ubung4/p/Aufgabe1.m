function [blocks, bases] = Aufgabe1()

clear
home
close all

%DCT generieren
base_size = 8; %4, 8 oder 16 DCT pro Base

% dct cell { base_size*base_size Matrix.dim(base_size,base_size) }
dct_cell = buildDCT2d(base_size);
%DCT2d_pict(dct_cell); % enable function call to get dct pictures on file system

J = imread("samplepic0353.ppm");
%pic = imread("icons8.ppm");
%konvertiereen in Gfraustufen
pic_int = ( J(:,:,1) .* 0.2126 ) + ( J(:,:,2) .*  0.7152 ) + ( J(:,:,3) .*  0.0722 );
%pic_int=double(pic)-127.5;
pic_blocks = Pic2Blocks(pic_int, base_size);
%blocks=pic_blocks;
%blockwise dct sum
dim_blocks = size(pic_blocks);

for i=1:dim_blocks(1)
  for j=1:dim_blocks(2)
    pic_blocks{i, j} = DCT_Sum(pic_blocks{i,j}, dct_cell);
  endfor
endfor
blocks=pic_blocks;
bases=dct_cell;
endfunction