Aufgabe 1

Die direkte Anwendung der Laufl ̈angencodierung (Run-Length Encoding, kurz RLE)auf  ein  Graustufenbild  (z.B.  8-Bit  codiert)  ist  nicht  sehr  effizient,  da  aufgrund  derBitaufl ̈osung (→256 M ̈oglichkeiten f ̈ur die Graustufen) eine Wiederholung eines exaktgleichen Graustufenwertes je nach Bildinhalt sehr unwahrscheinlich ist.
Eine bessere Option ist die Aufteilung des Bildes in sog. Bitplanes. Dabei wird das8-Bit codierte Bild in acht bin ̈are 1-Bit Ebenen konvertiert. Die erste Bit-Ebene wirddurch  Ersetzen  eines  jeden  Pixelswertes  durch  das  MSB  der  urspr ̈unglichen  8-Bit-Darstellung ersetzt. Die n ̈achste Bitebene wird durch Ersetzen jedes Pixelwerts durchdas zweith ̈ochste Bit der 8-Bit-Darstellung ersetzt, usw. Anschließend k ̈onnen die ein-zelnen Bitplanes jeweils mit der Laufl ̈angencodierung codiert werden, so dass schließ-lich acht separate RLE Codew ̈orter entstehen, die anschließend aneinandergereiht dasgesamte Codewort bilden.
1.  Schreiben Sie eine Funktion (rlencode.m) zur Umsetzung des RLE Algorithums.Die  Funktion  soll  eine  Bilddatei  mit  ASCII-codierten  Bildpunkten  im  PPM-Format als Eingabe nehmen und das Ergebnis in eine Datei rausschreiben.
Das zweidimensionale BildBwird zun ̈achst in seine Bitplanes separiert und ineinen  eindimensionalen  Vektorbkonvertiert.  Zusammengefasst  soll  der  Algo-rithmus dann folgendermaßen ablaufen:
Eingabe: Vektorbder L ̈angeN(kBit pro Eintrag)
Ausgabe: laufl ̈angencodierter Vektorcder L ̈angeM<N
Codierung:1
•erster Eintrag inb ist x
•zahle, wie oft x hintereinander auftritt→n
•f ̈urn= 1: schreibe x in c
•f ̈urn= 2: schreibe x,x in c
•f ̈urn>2: schreibe x,T,n in c 
-T ist eindeutiges Trennzeichen (kBit)
-n wird mit l≤k Bit codiert
•fahre mit n ̈achstem Eintrag x fort...
2.  W ̈ahlen Sie daskentsprechend der bekannten Beispiel-Bilddateien. F ̈urlw ̈ahlenSie einen sinnvollen Wert. Beachten Sie, dass davon in hohem Maße der Kom-pressionsgewinn abh ̈angt. Ber ̈ucksichtigen Sie ebenso die potentielle Problema-tik von L ̈angen, die ̈uber das durchlDarstellbare hinausgehen und die darauseventuell resultierende Sonderbehandlung.
Experimentieren  Sie  mit  unterschiedlichen  Werten  vonlund  fassen  Sie  Ih-re  Ergebnisse  tabellatisch  zusammen.  Wenden  Sie  dazu  Ihre  implementierteLaufl ̈aungencodierung auf die Bilddateisamplepic0498.ppm an und untersuchenSie das Codierungsergebnis auf die resultierende Kompressionsrate.



Aufgabe 2

Nachdem Sie in Aufgabe 1 eine Codierung gem ̈aß des Laufl ̈angenschemas implemen-tiert und analysiert haben, geht es nun darum aus den codierten Daten die urspr ̈ung-lichen wieder zur ̈uckzugewinnen.
1.  Implementieren Sie die entsprechende Decodierfunktion (rledecode.m) und wen-den  Sie  diese  auf  die  komprimierte  Bilddatei  an.  Stellen  Sie  sicher,  dass  dasErgebnis mit der Originaldatei ̈ubereinstimmt.
2.  Vergleichen Sie die hier erzielten Kompressionsraten mit denen der Huffman-Codierung aus Aufgabenblatt 2. Fassen Sie Ihre Ergebnisse schriftlich zusam-men.


Zusatzaufgabe (freiwillig)
Generell  erzielt  die  separate  Laufl ̈angencodierung  einzelner  Bitplanes  noch  bessereErgebnisse, wenn anstelle der ̈ublichen Bin ̈arcodierung der Pixelwerte die Darstellungals Graycode gew ̈ahlt wird.
1.  Begr ̈unden Sie theoretisch und schriftlich woher dieser Vorteil resultiert.2
2.  Modifizieren Sie Ihre Codier- und Decodierfunktionen aus Aufgabe 1 dahinge-hend, dass statt der Bin ̈arcodierung die Codierung der Bitplanes im Graycodeerfolgt. Die eigentliche Laufl ̈angencodierung bleibt davon unbeeinflusst.3.  Vergleichen Sie die Ergebnisse und dokumentieren Sie diese.