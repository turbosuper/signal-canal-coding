clear
home
close all
disp("Starting Program....")

I = imread ("samplepic0353.ppm");
J = imread ("samplepic0467.ppm");

 %:imshow(I

%Octave lifert die Bilder als 3 Dimensionale Matrizzen
%Erste Dimension der Matrize ist der X Wert des Pixels
%Zweiter Dimenstion der Matrize ist der Y Wer des Pixels
%Dritter Wert des Matrizze ist der Intensitaet verteilet in drei Farben - RGB,
%also die Matrizze hat drei Ebene in der z Axis

%ZB darstellen nur  Werte der 'R' -erste ebene des Z-s Achse, fuer alle Pixelen
%imshow(I(:,:,1)) %beduete: alle x, alle y, aber nur erste aus der Z
%AUFPASSEN - Die Werte fuer das Intensiviteat sind umgekhret '0' steht fuer Maximimum!

%man berechnet der Intensitaet aus der Formel (0.2126*R + 0.7152*G + 0.0722*B)

Iint = I(:,:,1) .* 0.2126 .+ I(:,:,2) .*  0.7152 .+ I(:,:,2) .*  0.0722;
Jint = J(:,:,1) .* 0.2126 .+ J(:,:,2) .*  0.7152 .+ J(:,:,2) .*  0.0722;
%das konvertiert das Bild in Greyscale. JEtzt jede Pixel hat eine Intensitaet

%um das auf hist darzustellen, muss man die Werte in einer Vektor speichern
IintVekt = Iint(:)';
figure(1)
hist(IintVekt,252);  
[nnI, xxI] = hist(IintVekt,252); %da es gibt nur 254 Werte der Intesitat, man kann es nicht
                               %auf 254 verteilen
prQI = nnI ./ 307200;           % Verhältnis des Auftretens zu den Klassen (Wahrscheinlichkeit)
H_I = EntropieH1(prQI)

JintVekt = Jint(:)';
figure(2)
hist(JintVekt,252);  
[nnJ, xxJ] = hist(JintVekt,252); 
prQJ = nnJ ./ 307200;          
H_J = EntropieH1(prQJ)

%Die Entrope von zweiter Bild ist deutlich kliener - es gibt viel mehr einfach wiesse
%Punkte da kann den Bild mit geringer anzahl der Bits kodiert werden