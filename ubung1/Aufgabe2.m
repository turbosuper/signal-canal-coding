clear
home
close all
disp("Starting Program....")

t = randi([0 1000], 1, 5000); %Vektor mit 5000 Werte Zufallszahlen aus der Bereich 0 bis 1000
figure(1)
hist(t,256);                  %Histogram mit 256 Klassen

[nn, xx] = hist(t,256);      %5000 Werte auf 100000 Symbolen verteilt

prQ = nn ./ 5000;           %Verhältnis des Auftretens zu den Klassen (Wahrscheinlichkeit)
%werteBereich = (0: 1 : 1000);

H = EntropieH1(prQ)
%Aufgabe 2.1
%Entropie ist  etwas kliener als 10 - das ist logisch, da M = 1000, und log2(1024) = 10 

%Aufgabe 2.2
[n2, x2] = hist(t, 1000);
prQ2 = n2 ./ 5000;
H2 = EntropieH1(prQ2)
%Je mehr klassen, desto mehr Symbole in M, desto grosser  - 
%Das beduetet um 256 unterschiedliche Symbole darzustellen braucht man weniger bits,
%als zb 1024 Symbole darzustellen. In dieser Fall est ist 8 vs 10 Bit/Symbol

%Aufgabe 2.3 @Jacek: t3 aber dann gleiches randi() werteBereich, da es vergl. mit existenter Entropie sein soll - so sieht man, wenn ich 2x randi aufrufe, dann H hinter komma unterschiedlich aber fast gleich, begründung in funktion der randi() funktion...
% @Jacek: Sonst habe ich verstanden, warum du es so  gemacht hast - sehr gut! habe ich nicht bemerkt, dass man es so machen muss...
t3 = randi([0 4096], 1, 5000);
[n3, x3] = hist(t, 4096);
prQ3 = n3 ./ 5000;
H3 = EntropieH1(prQ3)

% Entropie hängt immer von den Symbolauftreten in einer Nachricht ab. 
% Dh ändert sich diedie Nachrricht, ändert sich auch die Auftretenswahrscheinlichkeit der Symbole und damit auch die Entropie.
% Jedoch ändert sich nicht der Symbolumfang M im ganzen, weshalb H1 sich nur in einem kleinem Epsilon verändert.
% Die rand funktion bringt immer näherungtsweise ein gleiches ergebnis - bei gleichem Symbolumfang.

% Codewortlänge: @Jacek: das hier denke ich in Protokoll beschreiben - ich war mir nicht sicher, ob man L berechnen sollte

 Lu = log2(4096) %Codewortlange des Symbols - da alle Symbole mit gleiche Bits/Symbol dargestellt sind 
 L = sum(prQ3 .* Lu)