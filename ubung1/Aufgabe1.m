clear
home
close all

%Variabel Deklaration
a = 5
b = -2.3
t = [1 2.3 3.1 -3.4]    %Eindimensionales Zeilenvektor
v = [-3 2 1.9 -2] 
t1 = (0.1:0.002:3*pi());     %Eindimensionales Zeilenvektor automatisch erzeugt
M = [ 2.4 -5.4 3.3 22.1, 
      1 1 7.4  6,
      5 -3.4 8 8.1] ;    %Matrix 3x3
y = (-1:0.01:1);
%Grundoperationen
summe = a + b
vektorprodukt = t .* v  %Elemnt Multiplikation
b_betrag = abs(b)
t1_skalar = t1*a;
v_cubic = v .**3 ;       %jeder Elemnt ^3

zufall_D = randi([20, 30])          %Zufall Zahl in der bereich 20 bis 30

if (zufall_D > 25)
  disp("Zufall zahlf grosser als 25")
 else
  disp("Zahl groser.Funktion wird aufgerufen.")
  halbProdukt(t,zufall_D)
 endif
 
 sint1 = 12* sin(t1);
 save sin_of_t1.mat sint1
 
 t1_quadrat = t1 .**2;
 plot(t1,sint1,'o','Linewidth',34)
 hold
 plot(t1,t1_quadrat)
 
 %read a matrix from a txt file
%fid = fopen ("MatrixInternet.txt", "r");
load MatrixInternet.txt
MatrixInternet
%fclose (fid);