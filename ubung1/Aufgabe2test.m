clear
home
close all

t = randi([0 100],1, 500); %Vektor mit 5000 Werte Zufallszahlen aus der Bereich 0 bis 1000
figure(1)
hist(t,10);                  %Histogram mit 256 Klassen

figure(2)
[nn, xx] = hist(t,10);      %5000 Werte auf 100000 Symbolen verteilt

