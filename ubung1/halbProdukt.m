function ret_var = halbProdukt (vector, zahl)
  temp_a = zahl/2;
  ret_var = vector .* temp_a;
endfunction