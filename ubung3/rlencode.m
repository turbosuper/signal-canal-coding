%rlencode

% Funktion, der kodiert den Bild nach Run-length Encoding

% Funktion parameter:
%   Picture -  bild in pplm Format
%   FileName - Name der Datei
%   Bits    - Birbreite der kodierte Grauenstufen darstellung
% Ruckgabe
%   coded - koderitert Bild

function [coded, compressed] = rlencode (picture, file_name, bits)
  
  imshow(picture);
  title('Picture to Encode');

  #encoding
  [binary_coded] = convert_ppm_to_binary (picture, bits); 
  [bitplanes_vector] = convert_to_vector(binary_coded, bits);
  [coded] = vector_to_compressed(bitplanes_vector);
  
  compressed = (length(coded) / length(bitplanes_vector)) * 100;
  
  fid = fopen(file_name, "w");
  fdisp(fid, coded)
  fclose(fid) 
   
  #TEST
   
  #save the decimal colors - before encoding
  fid8 = fopen("decimal_colors_before_encoding.txt", "w");
  fdisp(fid8, picture);
  fclose(fid8);
  
  
  #save the binary coded colors - before encoding
  fid5 = fopen("initial_binary_colors.txt", "w");
  fdisp(fid5, binary_coded);
  fclose(fid5);
   
  #bitplanes vector
  fid2 = fopen ("notcoded.txt", "w");
  fdisp (fid2, bitplanes_vector);
  fclose(fid2);

 endfunction