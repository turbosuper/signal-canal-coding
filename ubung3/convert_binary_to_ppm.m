%convert_binary_to_ppm 

% Funktion, der konvertiert binar dartstellung zur decimal darstellung

% Funktion parameter:
%   pbinary_cell_array   - cell_array mit binar Werte fuer jede pixel,
%   width     - breite des Bildes
%   height    - hoehe dces Bildes
% Ruckgabe
%   ppm_out -  Graustufen Bild in ppm Format

 function [ppm_out] = convert_binary_to_ppm(binary_cell_array, width, height)
  len = length(binary_cell_array);
  i =1;
  ppm_out = zeros(width, height);
 while i < len
     for row = 1 : height
        for col = 1 : width
          dec_value = bin2dec(binary_cell_array{i});
          ppm_out(col,row) = dec_value;
          i++;
        endfor
    endfor
endwhile
 
 endfunction