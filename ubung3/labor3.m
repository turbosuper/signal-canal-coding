clear
home
close all
disp("Starting Program....");


J = imread("testbild.ppm");

%konvertiereen in Gfraustufen
Jint = ( J(:,:,1) .* 0.2126 ) + ( J(:,:,2) .*  0.7152 ) + ( J(:,:,3) .*  0.0722 );

#encode
[coded_pic, compress] = rlencode (Jint, "coded.txt", 8);
compress
#decode
[decoded_pic] = decode(coded_pic, 8, columns(Jint), rows(Jint)); 
#make diff --each-value
# Nothing displayed: no diffrence
# -else-
# output: row & col of diffrent value; value in Jint; value in decoded_pic
for row = 1 : rows(decoded_pic)
  for col = 1 : columns(decoded_pic)
    if(Jint(row,col) != decoded_pic(row,col) )
      row
      col
      Jint(row,col)
      decoded_pic(row, col)
    endif
  endfor
endfor
decPic_uint8 = uint8(decoded_pic);
figure(2);
imshow(decPic_uint8);
title('Picture decoded');
imwrite(decPic_uint8, 'decpic.ppm'); 