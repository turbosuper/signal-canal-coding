%rlencode

% Funktion, der dekodiert Bektor nach Run-length Encoding

% Funktion parameter:
%   EncodedCode -  koderitert Vektor 
% Ruckgabe
%   PictGray - dekodierter in Graustufen

function [DecodedVect] = rlendecode (EncodedCode)
  i = 1;
  len = length(EncodedCode);
  while (i < length(EncodedCode))
    if ( (i+2) <= len )
      window = EncodedCode(i:i+2);
      if(window(3) > 2)
        for AnzBits = 1 : window(3)
         DecodedVect(end+1) = window(1);
        endfor
        if((i+3) > len)
          break;
        endif
        i += 3;
      else
        if(window(1) == window(2))
          DecodedVect(end+1) = window(1);
          DecodedVect(end+1) = window(2);
          if((i+2) > len)
          break;
        endif
         i += 2;
        else
          DecodedVect(end+1) = window(1);
          if((i+1) > len)
          break;
        endif
          i += 1;
       endif
    endif
   else % EOF Handling - Lenghth(window) = 2
          DecodedVect(end+1) = window(1);      
          DecodedVect(end+1) = window(2);
         break;
    endif
  endwhile
  %add one extra when at the eof
  last_bit = DecodedVect(end);
  DecodedVect(end+1) = last_bit;


 endfunction