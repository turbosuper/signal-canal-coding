%decoded to binayr

% Funktion, der erzeugt biary verte fuer jede Farbe aus der bitplanes_vektor

% Funktion parameter:
%   single_vector -  Vector mit bits in die bitplanes Abbildungj
%   Bits    - Birbreite der kodierte Grauenstufen darstellung
% Ruckgabe
%   binary_values - Cell array, mit Farben binar kodiert

function [binary_values] = decoded_to_binary(single_vector, bits, height, width)
  binary_values ={};
  len = length(single_vector);
  pixel_count = (len/bits);
  k= 1;
  current_pixel = 1;
  i = 1;
  
  for current_pixel = 1 : pixel_count #work on each pixel separately
    for k = 0 : bits-1
      binary_digit = single_vector((pixel_count*(k))+current_pixel);
      binary_digit = dec2bin(binary_digit); #convert to string
      if (k == 0) #first bit,MSB
      binary_values{current_pixel} = binary_digit;
      else %add next bits if already bit 0 present
        old_string = binary_values{current_pixel};
        binary_values{current_pixel} = strcat(old_string, binary_digit);
      endif
      
     endfor 
  endfor
endfunction