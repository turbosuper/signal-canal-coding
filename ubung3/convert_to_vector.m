%convert_to_vector

% Funktion, der konvertiert den Bild zur Bitplanes auflosung

% Funktion parameter:
%   picture   - bild in Cell Array, mit Farben binar kodiert
%   bits      - anzahl der Bits pro Farbe
% Ruckgabe
%   bitplanes_vector - Vektor mit geordnete werte nach bitplanes

function [bitplanes_vector] = convert_to_vector(picture, bits)
    for k =1 : bits;
     for i =1 : columns(picture);
      for j = 1 : rows(picture);
        color_value = picture(j,i);
        current_bitplane_bit = color_value{1,1}(k);
        bitplanes_vector(end+1) = str2double(current_bitplane_bit);
      endfor
     endfor
    endfor
endfunction