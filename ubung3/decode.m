%decoded

% Funktion, der konvertiert Vektor nach RL kodierung zur PPM Bild

% Funktion parameter:
%   coded_vektor  - Vektor kodiert nach RL Kodierung
%   bit           - Anzahl der bits fuer Darstellung jede Farbe
%   height        - Greosse des Bildes
%   width         - Breite des Bildes
% Ruckgabe
%   ppm_out -  Graustufen Bild in ppm Format

function [ppm_pic] = decode(coded_vector, bits, width, height)

  #from RL to bitstream
  [decoded_vector] = rlendecode(coded_vector);
  
  #from bitstream to pixels
  [binary_pic] = decoded_to_binary(decoded_vector, bits, height, width);
  #pixes in decimal
  [ppm_pic] = convert_binary_to_ppm(binary_pic,width, height); 
  

  #TEST
  #show the binary code after decoding
  fid5 = fopen("decoded_binary_colors.txt", "w");
  fdisp(fid5, binary_pic);
  fclose(fid5);
  
  #show the decimal color values after decoding
  fid6 = fopen("decimal_color_vlaues_after_decoding.txt", "w");
  fdisp(fid6, ppm_pic);
  fclose(fid6);
    
  #decode the vector and save it to file
  fid3 = fopen ("decoded.txt", "w");
  fdisp (fid3, decoded_vector);
  fclose(fid3);
endfunction