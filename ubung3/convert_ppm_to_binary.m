%convert_ppm_to_binary 

% Funktion, der konvertiert jede Wert der Farbe zu binar kodierung

% Funktion parameter:
%   picture   - bild in ppm Format,
%   bits      - anzahl der Bits pro Farbe
% Ruckgabe
%   binary_cell_array - Cell Array mit die Graustufen in Bitplanes

function [binary_cell_array] = convert_ppm_to_binary(picture, bits)
   for i = 1 : rows(picture);
      for j = 1 : columns(picture);
       binary_cell_array{i,j} = dec2bin(picture(i,j),bits);
      endfor
   endfor
endfunction