%vector_to_compressed

% Funktion, der codiert Vector nach RL codierung

% Funktion parameter:
%   vector   - bild in Cell Array, mit Farben binar kodiert
% Ruckgabe
%   coded - Vektor mit kodierte Worter


 function [coded] = vector_to_compressed(vector)
  
  i= 1;
  
  while i < length(vector);
   coded(end+1) = vector(i);
   n = 0;
    do
      n++;
    until ((coded(end) != vector(i+n)) || ((i+n)>=length(vector))) ;
   
    if n == 2
      coded(end+1) = vector(i);
    elseif n > 2;
      coded(end+1) = vector(i+n);
      coded(end+1) = n;
    endif;
    i = i+n;
    
  endwhile;
 
 endfunction